class Article {
  final String text;
  final String domain;
  final String by;
  final String age;
  final int score;
  final int commentsCount;

  const Article(
      {this.text,
      this.domain,
      this.by,
      this.age,
      this.score,
      this.commentsCount});
}

final articles = [
  Article(
    text: 'Dataasdf retires from Cricket, its a huge loss for cricket world.',
    domain: 'google.com',
    by: 'Aditya Soni',
    age: '2 hours',
    score: 81,
    commentsCount: 128,
  ),
  Article(
    text: 'Sachin eweweretires from Cricket, its a huge loss for cricket world.',
    domain: 'google.com',
    by: 'Aditya Soni',
    age: '2 hours',
    score: 81,
    commentsCount: 128,
  ),
  Article(
    text: 'Sachin retirtryrtyes from Cricket, its a huge loss for cricket world.',
    domain: 'google.com',
    by: 'Aditya Soni',
    age: '2 hours',
    score: 81,
    commentsCount: 128,
  ),
  Article(
    text: 'Sachin retires from Criqwewqcket, its a huge loss for cricket world.',
    domain: 'google.com',
    by: 'Aditya Soni',
    age: '2 hours',
    score: 81,
    commentsCount: 128,
  ),
  Article(
    text: 'Sacasdsadhin retires from Cricket, its a huge loss for cricket world.',
    domain: 'google.com',
    by: 'Aditya Soni',
    age: '2 hours',
    score: 81,
    commentsCount: 128,
  ),
  Article(
    text: 'Sachin retires from Cricket, its a dfgdfghuge loss for cricket world.',
    domain: 'google.com',
    by: 'Aditya Soni',
    age: '2 hours',
    score: 81,
    commentsCount: 128,
  ),
];
