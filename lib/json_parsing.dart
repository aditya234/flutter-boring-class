import 'dart:convert' as converter;
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'serializers.dart';

part 'json_parsing.g.dart';

abstract class Article implements Built<Article, ArticleBuilder> {
  static Serializer<Article> get serializer => _$articleSerializer;

  int get id;

  bool get deleted;

  String get type;

  String get by;

  int get time;

  String get text;

  bool get dead;

  int get parent;

  int get poll;

  BuiltList<int> get kids;

  String get url;

  String get score;

  String get title;

  BuiltList<int> get parts;

  int get descendants;

  Article._();

  factory Article([updates(ArticleBuilder b)]) = _$Article;
}

List<int> parseTopStories(String jsonString) {
  return [];
//  final parsedJson = converter.jsonDecode(jsonString);
//  final listOfIds = List<int>.from(parsedJson);
//  return listOfIds;
}

Article parseArticle(String jsonString) {
  final parsedJson = converter.jsonDecode(jsonString);
  Article article = serializers.deserializeWith(Article.serializer, parsedJson);
  return article;
}
